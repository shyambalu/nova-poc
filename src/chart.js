var Chart = (function() {

    function Chart(id) {
        this._id = id;
        this.init();
    }


    Chart.prototype = {
        _canvas : null,
        _id : null,
        _cellSize : 20,
        _marginRight : 45,
        _marginBottom : 45,
        _xWidth :null,
        _months : ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
        init : function() {
            this._canvas = document.getElementById(this._id);
            if (!this._canvas)
                return;
            this._canvas.width = this._canvas.parentNode.clientWidth;
            this._canvas.height = 294;

            var _this = this;
            window.addEventListener('resize', function() {
                _this.onResize();
            });

            this.build();
        },
        build : function() {
            this._canvas.width = this._canvas.width;

            var ctx = this._canvas.getContext("2d");
            ctx.fillStyle = "#242426";
            ctx.fillRect(0, 0, this._canvas.width, this._canvas.height);

            this.buildGrid();
        },
        buildGrid : function() {

            var ctx = this._canvas.getContext("2d");
            var rowY = 0;
            var legend = 200;
            var monthsIndex = 0;
            while (rowY < this._canvas.height - this._marginBottom) {

                var colCount = 0;
                var color1 = '#28282a';
                var color2 = '#242426';
                var color = '#28282a';
                for (var colX = 0; colX < this._canvas.width - this._marginRight; colX += this._cellSize) {
                    if (colCount % 4 == 0)
                        color = color == color1 ? color2 : color1;
                    ctx.beginPath();
                    ctx.rect(colX, rowY, this._cellSize, this._cellSize);
                    ctx.fillStyle = color;
                    ctx.fill();
                    ctx.strokeStyle = "#333335";
                    ctx.lineWidth = 0.5;
                    ctx.stroke();
                    colCount++;
                    if (this._months[monthsIndex]) {
                        ctx.fillStyle = "#8e8e91";
                        ctx.font = "bold 11px Arial";
                        this._xWidth = (this._canvas.width - this._marginRight) / this._months.length;
                        var x = ( this._xWidth * monthsIndex) + (( this._xWidth - ctx.measureText(this._months[monthsIndex]).width) * 0.5);
                       
                        this.buildLegend(ctx, this._months[monthsIndex], x, this._canvas.height - 10);
                        monthsIndex++;
                    }
                }

                this.buildLegend(ctx, legend -= 10, colX + 5, rowY - 5);
                rowY += this._cellSize;
            }
            this.buildLegend(ctx, legend -= 10, colX + 5, rowY - 5);
        },
        buildLegend : function(ctx, value, x, y) {
             ctx.fillStyle = "#8e8e91";
                  ctx.font = "bold 11px Arial";
            this.createLabel(ctx, value, x, y);
        },
        createLabel : function(ctx, value, x, y) {

            console.log(ctx.measureText(value).width);
            ctx.fillText(value, x, y);
        },
        drawLines : function() {

        },
        onResize : function() {
            this._canvas.width = this._canvas.parentNode.clientWidth;
            this._canvas.height = 294;

            this.build();
        }
    };

    return Chart;
})();

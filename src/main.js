var Main = (function () {

    function Main() {

        this.RICS = ['AA.N', 'BAC.N', 'BA.N', 'BNKR.L', 'BOK.L', 'BRAM.L', 'BRLA.L', 'BRNE.L', 'CCL.L', 'CDI.L', 'AAPL.O'];

        this.NAMES = ['Alcoa Inc', 'Bank of America', 'Boeing Co', 'Bnkrs Inv Trust', 'Booker Group', 'Brammer PLC', 'Blckrck Lat Amer', 'Blckrck Nw Enr', 'Carnival', 'Candover Inv', 'Apple'];

        this.CATEGORIES = ['cat1', 'cat2', 'cat3'];

        this.init();


    }

    Main.prototype = {

        ricStackLength: 3,
        currentRic: 1,
        totalRics: 50,
        sidePanel: null,
        fullscreenCallback: null,
        panelToggle: null,
        panelOpen: true,
        chart: null,

        init: function () {
            var _this = this;
            window.addEventListener('load', function () {
                _this.onLoad();
            });
        },

        onLoad: function () {
            this.setAccordions();
            this.createSlider();
            this.populateTable();
            this.populateRics();
            this.animateTable();


            this.sidePanel = document.getElementsByClassName('sidebar')[0];
            this.panelToggle = document.getElementById('panelToggle');

            this.setListeners();


            this.chart = new Chart('chart');
        },

        setListeners: function () {
            var _this = this;

            this.sidePanel.addEventListener('click', this.handlePanel.bind(this), true);

            this.fullscreenCallback = function () {
                _this.fullscreen(_this);
            };

            $('.tableHolder')[0].addEventListener('scroll', this.handleTableScroll.bind(this));
            // document.body.addEventListener('click', this.fullscreenCallback);
        },

        setAccordions: function () {
            var checkboxes = document.getElementsByTagName('checkbox');

            for (var a = 0; a < checkboxes.length; a++) {
                checkboxes[a].childNodes[1].addEventListener('click', this.onCheckBoxClick, false);
            }
        },

        animateTable: function () {
            var rows        = this.table.children();
            var classes     = ["negative", "negative-highlight", "positive", "positive-highlight", "neutral"];

            var tr          = ~~ (Math.random() * rows.length);
            var _class      = ~~ (Math.random() * (classes.length - 2));


            var td0 = $($(rows[tr]).children()[2]);
            var td1 = $($(rows[tr]).children()[3]);
            var td2 = $($(rows[tr]).children()[4]);
            var td3 = $($(rows[tr]).children()[5]);

            var avail_classes = $.grep(classes, function (n) {
                return n.match(td1.attr('class'));
            }, true);

            // console.log(avail_classes, _class);

            var re = /(positive|negative|neutral)/g;
            var class_td = avail_classes[_class];
            var direction = class_td.match(re);

            var arrow = "";

            arrow = direction[0] === "positive" ? "up-arrow" : arrow;
            arrow = direction[0] === "negative" ? "down-arrow" : arrow;
            arrow = direction[0] === "neutral" ? "hide" : arrow;
            // console.log(arrow, direction[0])



            var progress = ((Math.random() * 0.8) + 0.1).toFixed(1);
            var last = ((Math.random() * 990)).toFixed(3);
            var net_chng = (Math.random() * 100).toFixed(2);
            var pct_chng = (Math.random() * 2).toFixed(2);


            var arrow_html = "<div class=\"" + arrow + "\"></div>";

            var pct_chng_html = pct_chng + "%" + "<meter class=\"" + direction + "\" min=\"0\" max=\"1\" value=\"" + progress + "\"></meter>";


            td0.html(arrow_html);
            td1.html(last).removeClass().addClass(class_td);
            td2.html(net_chng).removeClass().addClass(class_td);
            td3.html(pct_chng_html).removeClass().addClass(class_td);

            // console.log(td1, td2, td3);

            // setTimeout(this.animateTable.bind(this), 250);
        },

        handlePanel: function (e) {

            console.log(e.currentTarget, e.target);

            if (this.panelOpen && e.target === this.panelToggle) {
                this.sidePanel.classList.add('open');
                this.panelToggle.classList.add('closed');
                this.panelOpen = false;
            } else if (!this.panelOpen) {
                this.sidePanel.classList.remove('open');
                this.panelToggle.classList.remove('closed');
                this.panelOpen = true;
                e.stopPropagation();
                e.preventDefault();
            }
        },

        onCheckBoxClick: function (elem) {
            var input = elem.childNodes[1];
            input.checked = input.checked ? false : true;
            //
            // event.stoppropagation();
            // event.preventDefault();
            return false;
        },

        createSlider: function () {
            var slider = new RangeSlide();
            slider.data = [{
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }, {
                label: "10 SEP",
                value: 123456
            }];
            slider.callback = this.onRange;
            slider.build();
            slider.height(20);
            document.getElementById('rangeSlider').appendChild(slider.element);
            slider.arrange();
        },

        onRange: function () {
        },

        handleTableScroll: function (e) {
            console.log(e);
            console.log(e.target.scrollTop);

            thead_1 = $('.tableHolder > table').find('thead')[0].children[0];
            thead_2 = $('.tableHolder > table').find('thead')[1].children[0];

            thead_m = $('.tableHolder > table').find('thead')[1];

            if (e.target.scrollTop > 20) {

                thead_1.style.height = "18px";
                thead_2.style.height = "18px";
                thead_m.style.margin = "-19px 0 0 -1px";
            } else {
                thead_1.style.height = "28px";
                thead_2.style.height = "29px";
                thead_m.style.margin = "-29px 0 0 -1px";
            }
        },

        populateTable: function () {

            var html = ["<tr>"];

            for (var i = 0; i < 50; i++) {

                var rick_picker = ~~ ((Math.random() * this.RICS.length));

                var ric = this.RICS[rick_picker];
                html.push(this.td(ric));

                var comp_name = this.NAMES[rick_picker];
                var comp_html = "<a href=\"#\">" + comp_name + "</a>";
                html.push(this.td(comp_html));

                var arrow_pick = ~~ (Math.random() * 2) === 0 ? "down" : "up";
                var arrow = "<div class=\"" + arrow_pick + "-arrow\"></div>";
                html.push(this.td(arrow));

                var direction = arrow_pick == "down" ? "negative" : "positive";
                var highlight = ~~ (Math.random() * 2);
                var classname = highlight === 0 ? direction : direction + "-highlight";
                var progress = ((Math.random() * 0.8) + 0.1).toFixed(1);

                var last = ((Math.random() * 990)).toFixed(3);
                var net_chng = (Math.random() * 100).toFixed(2);
                var pct_chng = (Math.random() * 2).toFixed(2);

                var pct_chng_html = pct_chng + "%" + "<meter class=\"" + direction + "\" min=\"0\" max=\"1\" value=\"" + progress + "\"></meter>";

                html.push(this.td(last, classname));
                html.push(this.td(net_chng, classname));
                html.push(this.td(pct_chng_html, classname));

                var bid = ((Math.random() * 1000) + 1).toFixed(3);
                var ask = (bid - Math.random()).toFixed(3);
                var open = ((Math.random() * 1000) + 1).toFixed(3);

                html.push(this.td(bid));
                html.push(this.td(ask));
                html.push(this.td(open));

                for (var j = 0; j < 6; j++) {
                    var pick = ~~ (Math.random() * 2);

                    if (pick === 0) {
                        html.push(this.td("Y", "neutral-highlight"));
                    } else {
                        html.push(this.td("N"));
                    }
                }

                html.push("</tr>");

            }

            console.log($(".data > .tableHolder > table > tbody").html(html.join("")));

            this.table = $('.tableHolder > table > tbody');

            thead_1 = $('.tableHolder > table').find('thead')[0].children[0];
            thead_2 = $('.tableHolder > table').find('thead')[1].children[0];


            (function resizeFloatingHeader() {
                for (var i = 0; i < thead_1.children.length; i++)
                    thead_2.children[i].style.width = thead_1.children[i].clientWidth - 10 + 'px';
                setTimeout(resizeFloatingHeader, 25);
            })();
        },

        populateRics: function () {

            if (this.currentRic > this.totalRics) {
                return;
            }

            var elId = ~~ ((Math.random() * this.ricStackLength) + 1);
            // console.log(elId);

            var el = $("#" + "cat" + elId + " ul");

            el[0].parentNode.setAttribute("open", "");

            // el.html("");

            var _this = this;

            setTimeout(function () {
                _this.createRics(el);
            }, 100);
        },

        createRics: function (el) {
            var rick_picker = ~~ ((Math.random() * this.RICS.length));
            var color_pick = ~~ (Math.random() * 3);

            var ric = this.RICS[rick_picker];
            var color;

            if (color_pick === 0)
                color = "red";
            if (color_pick === 1)
                color = "green";
            if (color_pick === 2)
                color = "";

            var html = "<li class=\"" + color + "\"> <p>" + ric + "</p></li>";

            el.append(html);
            this.currentRic++;
            this.populateRics();
        },

        td: function (html, classname) {
            if (classname) {
                return "<td class=\"" + classname + "\">" + html + "</td>";
            } else {
                return "<td>" + html + "</td>";
            }
        },
    };

    return new Main();
})();
